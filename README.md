# Docker Postgres Freshprono

Prerequisites
-------------
1. install docker
1. install docker-compose
1. clone repository `git clone --recursive https://gitlab.com/couteau07/docker_postgres_freshprono.git`

Getting Started
---------------
1. start service: `docker-compose up`
